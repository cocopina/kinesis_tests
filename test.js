const AWS = require('aws-sdk');
const { Kinesis } = AWS;

AWS.config.region = 'us-east-1';

const kinesis = new Kinesis();

kinesis.putRecord({
    StreamName: 'events_testing',
    PartitionKey: '1',
    Data: JSON.stringify({
        group: 'TEST',
        type: 'SAHAR'
    })
}, (err) => {
    if (err) {
        throw new Error(err);
    }
})